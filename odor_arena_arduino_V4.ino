/*  odor_arena_arduino
    For interfacing with Python to control
    odor arena hardware

    created August 2020
    by W. Ryan Williamson
    ryan.williamson@ucdenver.edu

    Updates by the Optogenetics and Neural Engineering Core (bit.ly/onecore) on 4.Dec.2020
*/

// Pin definitions
//GND
const int tonePin = 0;      // Speaker
const int stepPinB = 1;      // Motor controls for reward delivery lane B, Black
const int stepPinC = 2;      // Motor controls for reward delivery lane C, Blue
const int stepPinA = 3;      // Motor controls for reward delivery lane A, blue (not on exit wire)

const int dirPin = 4;        // Motor controls for the direction of all reward lanes, Yellow

const int stepPinD = 5;      // Motor controls for reward delivery lane D, Red

const int fanPin = 6;

const int odorA = 7;    // Pin for odor valve, lane a
const int odorB = 8;    // Pin for odor valve, lane b
const int odorC = 9;    // Pin for odor valve, lane c
const int odorD = 10;    // Pin for odor valve, lane d
const int odorOutside = 11;   // Pin for odor channelled outside the chamber

const int lickDetectionA = 14;    // !!!Pin for lick detection, lane a
const int lickDetectionB = 15;    // Pin for lick detection, lane b
const int lickDetectionC = 16;    // Pin for lick detection, lane c
const int lickDetectionD = 17;    // Pin for lick detection, lane d

const int flowCtrlPin1 = 22; // Mass flow controller 1, TTL output
const int flowCtrlPin2 = 23; // Mass flow controller 2, TTL output

// Other constants used
int flowVal1 = 0;     // !!!Mass flow 1 default
int flowVal2 = 0;     // !!!Mass flow 2 default
int stepSpeed = 500;
int stepMag = 10;
int laneVal = 0;
int lanePin = 0;
int toneLen = 1000;        // Length in msec for how long to play the tone for
char rxChar = 'x';      // RXcHAR holds the received command
String rxStr = "x";   // RXcHAR holds additional values
int fanSpeed = 255;   // Fan speed between 0 and 255

//---------------- setup ---------------------------------------------
void setup() {
  pinMode(tonePin, OUTPUT);
  pinMode(stepPinA, OUTPUT);
  pinMode(dirPinA, OUTPUT);
  pinMode(stepPinB, OUTPUT);
  pinMode(dirPinB, OUTPUT);
  pinMode(stepPinC, OUTPUT);
  pinMode(dirPinC, OUTPUT);
  pinMode(stepPinD, OUTPUT);
  pinMode(dirPinD, OUTPUT);
  pinMode(fanPin, OUTPUT);
  pinMode(odorA, OUTPUT);
  pinMode(odorB, OUTPUT);
  pinMode(odorC, OUTPUT);
  pinMode(odorD, OUTPUT);
  pinMode(odorOutside, OUTPUT);
  pinMode(lickDetectionA, INPUT);
  pinMode(lickDetectionB, INPUT);
  pinMode(lickDetectionC, INPUT);
  pinMode(lickDetectionD, INPUT);
  pinMode(flowCtrlPin1, OUTPUT);
  pinMode(flowCtrlPin2, OUTPUT);

  digitalWrite(odorOutside, HIGH); //  Default to odor going outside chamber

  Serial.begin(115200); // Open serial port (115200 bauds).
  Serial.flush();     // Clear receive buffer.
  rxStr.reserve(200);
}


//--------------- loop -----------------------------------------------
void loop() {
  if (Serial.available() > 0) {        // Check receive buffer.
    rxChar = Serial.read();
  }
  if (digitalRead(lickDetectionA) == HIGH) {
    Serial.println('LA');              // !!!Should I only use one character?
  }
  if (digitalRead(lickDetectionB) == HIGH) {
    Serial.println('LB');
  }
  if (digitalRead(lickDetectionC) == HIGH) {
    Serial.println('LC');
  }
  if (digitalRead(lickDetectionD) == HIGH) {
    Serial.println('LD');
  }

  if (rxChar != 'x') {
    switch (rxChar) {
      case 'T':
        tone(tonePin, 5000, toneLen);  // Begin a tone which automatically stops after toneLen msec
        break;

      case 'A':
        analogWrite(fanPin, fanSpeed);
        break;

      case 'B':
        digitalWrite(fanPin, LOW);
        break;

      case 'C':
        // Makes pulses for turning stepper motor
        for (int x = 0; x < 200; x++) {
          digitalWrite(stepPin, HIGH);
          delayMicroseconds(stepSpeed);
          digitalWrite(stepPin, LOW);
          delayMicroseconds(stepSpeed);
        }
        break;

      case 'D':
        digitalWrite(dirPin, HIGH); // Enables the motor to move in a particular direction
        // Makes pulses for turning stepper motor
        for (int x = 0; x < 200; x++) {
          digitalWrite(stepPin, HIGH);
          delayMicroseconds(stepSpeed);
          digitalWrite(stepPin, LOW);
          delayMicroseconds(stepSpeed);
        }
        digitalWrite(dirPin, LOW); // Reset the pin
        break;

      case 'F':  // setting flow magnitude for mass flow controller
        if (Serial.available() > 0) {
          rxStr = Serial.readString();            // Save character received.
          flowVal1 = rxStr.toInt();
        }
        analogWrite(flowCtrlPin1, flowVal1);
        break;

      case 'G':  // setting flow magnitude for mass flow controller
        if (Serial.available() > 0) {
          rxStr = Serial.readString();            // Save character received.
          flowVal2 = rxStr.toInt();
        }
        analogWrite(flowCtrlPin2, flowVal2);
        break;

      case 'S':
        if (Serial.available() > 0) {
          rxStr = Serial.readString();            // Save character received.
          stepMag = rxStr.toInt();
        }
        break;

      case 'R':
        if (Serial.available() > 0) {
          rxStr = Serial.readString();            // Save character received.
          laneVal = rxStr.toInt();                // Lane to dispense reward

          if (laneVal == 1) {                     // Sigh, now we are calling lanes numbers? Whatever. 1 = A, 2 =B, you know
            lanePin = rewardA;
          }
          else if (laneVal == 2) {
            lanePin = rewardB;
          }
          else if (laneVal == 3) {
            lanePin = rewardC;
          }
          else if (laneVal == 4) {
            lanePin = rewardD;
          }

          digitalWrite(lanePin, HIGH);

          digitalWrite(dirPin, HIGH); // Enables the motor to move in a particular direction
          // Makes pulses for turning stepper motor
          for (int x = 0; x < stepMag; x++) {
            digitalWrite(stepPin, HIGH);
            delayMicroseconds(stepSpeed);
            digitalWrite(stepPin, LOW);
            delayMicroseconds(stepSpeed);
          }
        }

        digitalWrite(lanePin, LOW);

        break;

      case 'O':
        rxStr = Serial.readString();            // Save character received.
        laneVal = rxStr.toInt();    // Lane to dispense odor

        if (laneVal == 1) {
          lanePin = odorA;
        }
        else if (laneVal == 2) {
          lanePin = odorB;
        }
        else if (laneVal == 3) {
          lanePin = odorC;
        }
        else if (laneVal == 4) {
          lanePin = odorD;
        }

        digitalWrite(lanePin, HIGH);
        digitalWrite(odorOutside, LOW);
        break;

      case 'P':
        digitalWrite(odorOutside, HIGH);
        digitalWrite(odorA, LOW);
        digitalWrite(odorB, LOW);
        digitalWrite(odorC, LOW);
        digitalWrite(odorD, LOW);
        break;
    }
  }
  rxChar = 'x';
  Serial.flush();                    // Clear receive buffer.
}
// End of the Sketch.
